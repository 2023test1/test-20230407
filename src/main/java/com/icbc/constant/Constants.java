package main.java.com.icbc.constant;

public class Constants {
    // 苹果
    public static final String APPLE_STR = "苹果";
    // 草莓
    public static final String STRAWBERRY_STR = "草莓";
    // 芒果
    public static final String MANGO_STR = "芒果";
}
