package main.java.com.icbc.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class Fruit implements Serializable {
    private String name;

    private BigDecimal price;

    private int num;

    public Fruit(String name, BigDecimal price, int num) {
        this.name = name;
        this.price = price;
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
