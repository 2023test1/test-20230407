package main.java.com.icbc.compute;

import main.java.com.icbc.dto.Fruit;
import main.java.com.icbc.inter.AppleFactory;
import main.java.com.icbc.inter.MangoFactory;
import main.java.com.icbc.inter.StrawberryFactory;

import java.math.BigDecimal;

public class Result {
    public static void main(String[] args) {
        try {
            // 初始化
            Fruit apple = new AppleFactory().createFruit();
            Fruit strawberry = new StrawberryFactory().createFruit();
            Fruit mango = new MangoFactory().createFruit();

            // 顾客A所购买商品的总价
            BigDecimal sumA = apple.getPrice().multiply(new BigDecimal(apple.getNum())).add(strawberry.getPrice().multiply(new BigDecimal(strawberry.getNum()))).setScale(4, BigDecimal.ROUND_HALF_UP);
            System.out.println(sumA);

            // 顾客B所购买商品的总价
            BigDecimal sumB = apple.getPrice().multiply(new BigDecimal(apple.getNum())).add(strawberry.getPrice().multiply(new BigDecimal(strawberry.getNum()))).add(mango.getPrice().multiply(new BigDecimal(mango.getNum()))).setScale(4, BigDecimal.ROUND_HALF_UP);
            System.out.println(sumB);

            // 顾客C所购买商品的总价
            BigDecimal sumC = apple.getPrice().multiply(new BigDecimal(apple.getNum())).add(strawberry.getPrice().multiply(new BigDecimal(strawberry.getNum())).multiply(new BigDecimal(0.8))).add(mango.getPrice().multiply(new BigDecimal(mango.getNum()))).setScale(4, BigDecimal.ROUND_HALF_UP);
            System.out.println(sumC);

            // 顾客D所购买商品的总价
            BigDecimal sumD = apple.getPrice().multiply(new BigDecimal(apple.getNum())).add(strawberry.getPrice().multiply(new BigDecimal(strawberry.getNum())).multiply(new BigDecimal(0.8))).add(mango.getPrice().multiply(new BigDecimal(mango.getNum()))).setScale(4, BigDecimal.ROUND_HALF_UP);
            sumD = sumD.compareTo(new BigDecimal(100)) == 1 ? sumD.subtract(new BigDecimal(10)) : sumD;
            System.out.println(sumD);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
