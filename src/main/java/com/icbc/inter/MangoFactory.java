package main.java.com.icbc.inter;

import main.java.com.icbc.constant.Constants;
import main.java.com.icbc.dto.Fruit;

import java.math.BigDecimal;

public class MangoFactory implements FruitFactory {
    @Override
    public Fruit createFruit() {
        return new Fruit(Constants.MANGO_STR, new BigDecimal(20), 3);
    }
}
