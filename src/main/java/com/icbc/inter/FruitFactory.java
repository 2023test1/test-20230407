package main.java.com.icbc.inter;

import main.java.com.icbc.dto.Fruit;

public interface FruitFactory {
    Fruit createFruit();
}
