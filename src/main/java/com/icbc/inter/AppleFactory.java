package main.java.com.icbc.inter;

import main.java.com.icbc.constant.Constants;
import main.java.com.icbc.dto.Fruit;

import java.math.BigDecimal;

public class AppleFactory implements FruitFactory {
    @Override
    public Fruit createFruit() {
        return new Fruit(Constants.APPLE_STR, new BigDecimal(8), 3);
    }
}
